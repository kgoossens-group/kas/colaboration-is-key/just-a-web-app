package org.acme;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("/api")
public class APIResource {
    // Represents the ‘homepage.title’ and ‘homepage.message’ configuration properties injected by Quarkus
    @ConfigProperty(name = "homepage.title")
    String title;

    @ConfigProperty(name = "homepage.message")
    String message;

    @GET
    @Path("/title")
    @Produces(MediaType.TEXT_PLAIN)
    public String getTitle() {
        return title;
    }

    @GET
    @Path("/message")
    @Produces(MediaType.TEXT_PLAIN)
    public String getMessage() {
        return message;
    }
}