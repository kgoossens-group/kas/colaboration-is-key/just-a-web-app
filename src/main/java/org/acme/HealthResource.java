package org.acme;


import java.io.InputStream;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/heath")
public class HealthResource {


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String ping10(String url) throws Exception {
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec(new String[] {"sh", "-c", "ping -c 10 " + url});
        int result = proc.waitFor();
        if (result != 0) {
          System.out.println("process error: " + result);
        }
        
        InputStream in = 
            (result == 0) ? proc.getInputStream() : proc.getErrorStream();
        
        StringBuilder stringBuilder = new StringBuilder();
        int c;
        while ((c = in.read()) != -1) {
            stringBuilder.append((char) c);
        }


        return stringBuilder.toString();
      }
}
