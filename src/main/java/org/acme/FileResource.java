package org.acme;

import java.io.InputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/files")
public class FileResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String listFiles(String dir) throws Exception {
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec(new String[] {"sh", "-c", "ls " + dir});
        int result = proc.waitFor();
        if (result != 0) {
          System.out.println("process error: " + result);
        }
        
        InputStream in = 
            (result == 0) ? proc.getInputStream() : proc.getErrorStream();
        
        StringBuilder stringBuilder = new StringBuilder();
        int c;
        while ((c = in.read()) != -1) {
            stringBuilder.append((char) c);
        }

        return stringBuilder.toString();
      }
}
